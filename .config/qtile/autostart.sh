#!/usr/bin/env bash 

COLORSCHEME=DoomOne


#lxsession &
### AUTOSTART PROGRAMS ###
picom --daemon &
copyq &
#/usr/bin/emacs --daemon &
#bash -c "sleep 3 && picom -b --experimental-backends"
#find $HOME/.wallpapers/ -type f | shuf -n 1 | xargs xwallpaper --stretch &
#nm-applet &
#xwallpaper --stretch /home/tulashi/.wallpapers/0099.jpg &
#"$HOME"/.screenlayout/layout.sh &
$HOME/.local/bin/random-wallpaper &
xinput set-prop 'ELAN06FA:00 04F3:327E Touchpad' "libinput Tapping Enabled" 1
xinput set-prop 'ELAN06FA:00 04F3:327E Touchpad' 'libinput Natural Scrolling Enabled' 1
picom &
#xscreensaver &
exec gsettings set org.gnome.desktop.interface color-scheme 'prefer-dark'
# conky -c "$HOME"/.config/conky/qtile/01/"$COLORSCHEME".conf || echo "Couldn't start conky."

### UNCOMMENT ONLY ONE OF THE FOLLOWING THREE OPTIONS! ###
# 1. Uncomment to restore last saved wallpaper
#xargs xwallpaper --stretch < ~/.cache/wall &
# 2. Uncomment to set a random wallpaper on login
# 3. Uncomment to set wallpaper with nitrogen
# nitrogen --resto &
